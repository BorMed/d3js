import * as html2canvasSource from '../../libs/html2canvas/html2canvas';

// @ts-ignore
export const html2canvas: Html2CanvasStatic = html2canvasSource;
