import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import pdfMake, { Content, TDocumentDefinitions } from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { html2canvas } from './html2canvas';
import { Season } from './models/example.enum';
import { difference } from 'lodash';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

// import html2canvas from 'html2canvas';

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements AfterViewInit, OnInit {
  @ViewChild('input')
  input: ElementRef;
  previousValue = '';

  get label(): string {
    return this._label;
  }

  set label(value: string) {
    this._label = value;
  }

  @ViewChild('chart')
  chart: ElementRef;
  @ViewChild('img')
  img: ElementRef;
  @ViewChild('canvas')
  canvas: ElementRef;
  @ViewChild('qqq')
  qqq: ElementRef;
  public ch: c3.ChartAPI;

  season: Season;
  data1 = [
    '2013-01-01', '2013-01-02', '2013-01-03',
    '2013-01-04', '2013-01-05', '2013-01-06',
    '2013-01-07', '2013-01-08', '2013-01-09',
    '2013-01-10', '2013-01-11', '2013-01-12'
  ];
  defaultInitZoom: any;

  counter = 0;
  maxCounter = 5;

  title = 'learn-d3-c3js';
  private data2 = [30, 200, 100, 400, 150, 250, 30, 200, 100, 400, 150, 250, 300, 150];
  zoomDefault: any;
  // tslint:disable-next-line:variable-name
  private _label = 'label';
  form: FormGroup;
  maskOptions: any = {
    mask: Number,
    min: 0,
    max: 100
  };

  constructor(
    private element: ElementRef,
    private http: HttpClient) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      'name': new FormControl('asdf')
    });
  }

  ngAfterViewInit(): void {
    this.createChart();
    this.zoomDefault = this.ch.zoom;
    //


  }

  unzoom() {
    // this.ch.unzoom();
    // this.ch.zoom();
    // // this.ch.zoom = this.zoomDefault;
    // this.ch.unload();
    // this.ch.load({
    //   columns: [
    //     ['x', ...this.data1],
    //     ['sample_2', ...this.data2],
    //   ]
    // });
    // // this.ch.revert();
    this.ch.destroy();
    this.createChart();

  }

  toggleData1() {
    this.ch.toggle('data1');
  }

  createPdf() {
    const svg = this.element.nativeElement.querySelector('svg');
    // const svg = this.element.nativeElement.querySelector('svg');
    const defs = document.createElement('defs');
    // defs.innerHTML = style;

    svg.append(defs);
    svg.querySelectorAll('.c3 path').forEach(
      (p) => {
        p.style.fill = 'none';
        // p.style.stroke = '#000';
      }
    );
    svg.querySelectorAll('.c3 line').forEach(
      (p) => {
        p.style.fill = 'none';
      }
    );

    svg.querySelectorAll('.domain').forEach(
      (d) => {
        d.style.stroke = '#000';
        d.querySelectorAll('text').forEach(
          (t) => {
            t.style.fontSize = '0';
          }
        );
      }
    );

    svg.querySelectorAll('.c3-axis.c3-axis-x text').forEach(
      (t) => {
        t.style.fontSize = '10px';
        t.style.fontFamily = 'Times New Roman';
      }
    );

    html2canvas(svg, {svgRendering: true}).then(canvas => {
      // document.body.appendChild(canvas);
      let dt = canvas.toDataURL('image/png', 1);
      dt = dt.replace(/^data:image\/[^;]*/, 'data:application/octet-stream');
      dt = dt.replace(
        /^data:application\/octet-stream/,
        'data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20filename=Canvas.png'
      );
      const content: Content = [
        'sdfasdfas',
        {
          image: dt,
          // fit: [100, 100],
          width: 400
        },
        'sdafasdfasdfasdfasd'
      ];
      const docDefinition: TDocumentDefinitions = {
        pageSize: 'A4',
        content
      };

      pdfMake.createPdf(docDefinition).open();
    });

  }

  private createChart() {
    this.ch = c3.generate({
      bindto: this.chart.nativeElement,
      data: {
        xs: {
          versicolor: 'versicolor_x',
        },
        // iris data from R
        columns: [
          ['versicolor_x', 3.2, 3.2, 3.1, 2.3, 2.8, 2.8],
          ['versicolor', 1.4, 1.5, 1.5, 1.3, 1.5, 1.3],
        ],
        // labels: {
        //   format: v => {console.log(v); return ''; }
        // },
        type: 'bar'
      },
      axis: {
        x: {
          label: this.label,
          tick: {
            fit: false
          }
        },
        y: {
          label: 'Petal.Width'
        }
      }
    });

  }

  // private createChart() {
  //   this.ch = c3.generate({
  //     bindto: this.chart.nativeElement,
  //     size: {
  //       height: 300,
  //       width: 800
  //     },
  //     data: {
  //       x: 'x',
  //       columns: [
  //         ['x', ...this.data1],
  //         ['sample_2', ...this.data2],
  //         // For 2 color chart
  //         // ['sample', 30, 200, 100, 400, 150]
  //       ],
  //       // type: 'bar'
  //     },
  //     zoom: {
  //       enabled: true,
  //       // onzoom(domain: [number, number]): void {
  //       //
  //       // },
  //       onzoom: (domain: [number, number]): void => {
  //         // console.log('counter: ', this.counter);
  //         if (this.counter > this.maxCounter) {
  //           this.counter = 0;
  //           // setTimeout(() => this.unzoom(), 0);
  //           // this.unzoom();
  //           return;
  //         }
  //
  //         this.counter++;
  //         console.log('domain', domain);
  //         console.log('chart', this.ch);
  //       }
  //     },
  //     axis: {
  //       x: {
  //         type: 'timeseries',
  //         tick: {
  //           values: ['2013-01-05', '2013-01-10'],
  //           // culling: {
  //           //   max: 8 // the number of tick texts will be adjusted to less than this value
  //           // },
  //           // count: 3,
  //           format: '%e %b %y'
  //         },
  //         label: {
  //           text: 'X Label',
  //           position: 'outer-center'
  //         }
  //       },
  //       y: {
  //         label: {
  //           text: 'Y Label',
  //           position: 'outer-center'
  //         }
  //       }
  //     },
  //     grid: {
  //       x: {
  //         show: true
  //       },
  //       y: {
  //         show: true
  //       }
  //     }
  //   });
  //   this.defaultInitZoom = (this.ch).internal;
  //   console.log('initZoom', this.defaultInitZoom);
  // }
  changeLabel() {
    const map = new Map();
    map.set('12', 189).set('13', 400);
    const [pp, as] = map;
    console.log(...map);
    // @ts-ignore
    // const {, 189} = Object.fromEntries(map);
    console.log(Object.fromEntries(map));
    const p = {
      ...Object.fromEntries(map)
    }
    // console.log(one, two);
    // const {'12', '34'} = {
    //   12: 10,
    //   34: 100
    // };
    // console.log(pp);
    // console.log(as);
    this._label = 'asdfasdfasdf';
  }


  onInput(event: InputEvent) {
    console.log('---', event);
    // const inchRegexp = //;
    const regexp = /[\d.]/;
    const input = this.input.nativeElement as HTMLInputElement;
    const value = input.value;
    console.log(value);
    const newSimbol = difference(Array.from(input.value), Array.from(this.previousValue));
    if (!newSimbol.length) {
      return;
    }
    if ((input.value.length === 1 && newSimbol[0] === '.')) {
      input.value = '';
      return;
    }
    if (!regexp.test(newSimbol[0])) {
      input.value = this.previousValue;
      return;
    }

    const arr = value.split('.');
    if (arr.length > 2) {
      input.value = this.previousValue;
      return;
    }
    // input.value = value.replace(regexp, '');
    if (arr.length === 2) {

    } else {

    }


    console.log(Array.from(this.previousValue));
    console.log(Array.from(value));
    console.log();

    let foots;
    let inchs;
    // if (arr.length) {
    // }
    this.previousValue = '' + input.value;
  }

  doPost() {
    // console.log(JSON.stringify(this.form.getRawValue()));
    this.http.post('http://localhost:8090/cubes/tt', JSON.stringify(this.form.getRawValue())).subscribe();
  }
}
