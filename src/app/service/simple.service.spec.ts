import { anyNumber, anything, capture, instance, mock, reset, verify, when } from 'ts-mockito';
import { CalculatorService } from './calculator.service';
import { SimpleService } from './simple.service';

interface IMock<T> {
  mock: T;
  instance?: T;
}

describe('simple service', () => {
  const mockCalculatorService: CalculatorService = mock<CalculatorService>(CalculatorService);
  let simpleService: SimpleService;

  beforeAll(() => {
    simpleService = new SimpleService(instance(mockCalculatorService));
  });

  beforeEach(() => {
    reset(mockCalculatorService);
  });

  it('should right doing stuff if pass right params', () => {
    const stubNumber = 10;

    when(mockCalculatorService.plus(anyNumber(), anyNumber())).thenReturn(60);

    simpleService.doSmth(stubNumber);
    // simpleService.doSmth(stubNumber);
    verify(mockCalculatorService.plus(anything(), anyNumber())).once();

    const params = capture(mockCalculatorService.plus).first()[0];

    console.log('params: ', params);
  });

  fit('other test', () => {
    reset(mockCalculatorService);
    when(mockCalculatorService.plus(anyNumber(), anyNumber())).thenReturn(5000).thenReturn(1000);
    simpleService.doSmth(100);
    simpleService.doSmth(100);
  });
});
