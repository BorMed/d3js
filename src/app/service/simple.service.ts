import { Injectable } from '@angular/core';
import { CalculatorService } from './calculator.service';

@Injectable()
export class SimpleService {

  constructor(private calculator: CalculatorService) {
  }

  public doSmth(value: number) {
    console.log('from doStuff()', this.calculate(value));
  }

  private calculate(value: number): number {
    return this.calculator.plus(value, value);
  }
}
