import { Injectable } from '@angular/core';

@Injectable()
export class CalculatorService {
  constructor() {
  }

  plus(one: number, two: number) {
    return one + two;
  }
}
