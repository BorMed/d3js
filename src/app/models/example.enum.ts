export enum Season {
  WINTER = 'winter',
  SPRING = 'spring',
  SUMMER = 'summer',
  FELL = 'fell'
}

// @ts-ignore
export namespace Season {
  export function getDescription(season: Season): string {
    if (season === Season.WINTER) {
      return 'winter!!!';
    } else if (season === Season.SPRING) {
      return 'spring !!! girls';
    } else if (season === Season.SUMMER) {
      return 'summer!! bear!!';
    } else if (season === Season.FELL) {
      return 'fell :(';
    } else {
      return 'empty';
    }
  }
}
