import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CalculatorService } from './service/calculator.service';
import { SimpleService } from './service/simple.service';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { IMaskModule } from 'angular-imask';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    IMaskModule,
  ],
  providers: [
    CalculatorService,
    SimpleService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
